#!/usr/bin/env python

from distutils.core import setup

setup(
    name='population',
    version='0.1.1',
    packages=['population'],
    package_dir={'population': '.'}
)
