A collection of parametric binary black hole mass/spin population models.

These are formatted to be consistent with the [Bilby](https://git.ligo.org/Monash/bilby) [hyper-parameter inference package](https://monash.docs.ligo.org/bilby/hyperparameters.html).

Most of the models implemented are derived from models presented in one of:
- [Talbot & Thrane (2017)](https://arxiv.org/abs/1704.08370)
- [Talbot & Thrane (2018)](https://arxiv.org/abs/1801.02699)
- [Wysocki et al. (2018)](https://arxiv.org/abs/1805.06442)

Automatically generated docs can be found [here](https://colm.talbot.docs.ligo.org/population/).